const routes = require('express').Router();
const AppController = require('../app/controllers/appController');

routes.get('/', AppController.index);

module.exports = routes;
