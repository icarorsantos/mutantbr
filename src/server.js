require('./services/es');
const app = require('./app');

const {
  env: { PORT },
} = process;

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
