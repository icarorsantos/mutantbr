const API = require('../../services/api');

class AppController {
  async index(req, res) {
    try {
      const websites = [];
      const users = [];
      const suites = [];

      const { data } = await API.get('users');

      data.sort((a, b) => {
        const isGreater = a.name > b.name ? 1 : -1;
        return a.name === b.name ? 0 : isGreater;
      });

      data.map((user) => {
        const { website, name, email, company, address } = user;

        websites.push(website);
        users.push({ name, email, company });

        if (address.suite.toLowerCase().includes('suite')) {
          suites.push(user);
        }

        return true;
      });

      return res.send({ websites, users, suites });
    } catch (error) {
      /* istanbul ignore next */
      return res.status(500).send({ error });
    }
  }
}

module.exports = new AppController();
