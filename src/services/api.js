const axios = require('axios');

const API = axios.create({
  baseURL: 'https://jsonplaceholder.typicode.com',
});

module.exports = API;
