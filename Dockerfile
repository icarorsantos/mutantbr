FROM node:14.7.0-buster

WORKDIR /app
COPY . .
RUN yarn
RUN cp .env.example .env

ENTRYPOINT [ "yarn" ]
CMD [ "start" ]

EXPOSE 8080