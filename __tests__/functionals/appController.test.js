const request = require('supertest');
const app = require('../../src/app');

describe('App controller tests', () => {
  it('should get correctly result', async () => {
    const { statusCode, body } = await request(app).get('/');

    expect(statusCode).toBe(200);
    expect(body.websites).toBeTruthy();
    expect(body.users).toBeTruthy();
    expect(body.suites).toBeTruthy();
  });
});
