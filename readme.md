## MutantBR Backend Challenge

This project is a part of MutantBR challenge.  
To run, you can follow the instructions bellow:

### To run on Vagrant:

```
vagrant up
```

and access [http://localhost](http://localhost) on browser.

### To run locally

You will need install dependencies and create .env file.

```
cp .env.example .env
yarn
yarn start
```

and access [http://localhost:8080](http://localhost:8080) on browser.

### To run tests

You will need install dependencies and create .env file.

```
cp .env.example .env
yarn
yarn test
```
